package com.dz.exo.gold.demo

class CMConstants {
    companion object {
        const val ADD_APP_SESSION_METADATA = 1
        const val ADD_PLAYER_SESSION_METADATA = 2

        const val READ_APP_SESSION_METADATA = 3
        const val READ_PLAYER_SESSION_METADATA = 4


        const val DELETE_APP_SESSION_METADATA = 5
        const val DELETE_PLAYER_SESSION_METADATA = 6


        const val UPDATE_APP_SESSION_METADATA = 7
        const val UPDATE_PLAYER_SESSION_METADATA = 8

        const val ADD_CUSTOM_METADATA_FOR_EXPLICINT_EVENT = 9
        const val DELETE_CUSTOM_METADATA_FOR_EXPLICINT_EVENT = 10
    }
}